window.onload = () => {
  jwt = localStorage.getItem("jwt");
  if (!jwt) {
    location.replace("/login");
    return;
  }
  socket = io.connect("http://localhost:3000");
  gameSocket = socket;

  socket.on("reconnect", () => {
    gameSocket = socket;
  });

  socket.on("wait", async ({ timeToGame }) => {
    await handleWaitEvt(timeToGame);
  });

  socket.on("start", ({ timeToGame, players }) => {
    console.log("received start evt", timeToGame, players);
    handleStartEvt(timeToGame, players);
  });

  socket.on("busy", () => {
    handleBusyEvt();
  });

  socket.on("progress", payload => {
    handleProgressEvt(payload);
  });

  socket.on("disconnect", () => {
    handleDisconnectEvt();
  });

  socket.on("finish", payload => {
    handleFinishEvt(payload);
  });
};

/* shared variables */

const pageBlockIds = [
  "progress",
  "text",
  "waiting",
  "busy",
  "time",
  "disconnected"
];

let game = {};
let waiting = false;
let startedGame = 0;
let jwt = "";
let gameSocket = null;
let finished = false;

/*event handlers */
handleWaitEvt = async timeToGame => {
  if (!waiting) {
    waiting = true;
    displayPageBlocks(["waiting"]);
    if (!game.gameText) {
      game.gameText = await fetchGameText();
      game.correctSymbols = 0;
      game.errorSymbols = 0;
    }
  }
  updateTime(timeToGame, "time-before-race");
  // get text from route text
};

handleStartEvt = (timeToGame, players) => {
  waiting = false;
  startedGame = Date.now();
  updateTime(timeToGame, "time-until-end");
  updateGameText(game);
  createPlayersView(players);
  displayPageBlocks(["progress", "text", "time"]);
  document.addEventListener("keydown", handleTyping);

  gameSocket.emit("accept-start", {
    token: jwt
  });
};

handleProgressEvt = ({ players }) => {
  console.log("UPDATE ARRIVED", players);
  displayUpdates(players);
};

handleFinishEvt = payload => {
  // handle game end
  gameHappened = true;
  game = {};
  document.removeEventListener("keydown", handleTyping);
  gameSocket.emit("accept-finish", {
    token: jwt
  });
};

handleDisconnectEvt = () => {
  game = {};
  waiting = false;
  startedGame = null;
  gameSocket = null;
  displayPageBlocks(["disconnected"]);
};

handleBusyEvt = () => {
  waiting = false;
  startedGame = null;
  displayPageBlocks(["busy"]);
};

/* helpers and business logic */

formatTime = time => {
  const minutes = Math.floor(time / 60000);
  const minutesText = minutes ? `${minutes}m ` : "";
  const seconds = Math.ceil(time / 1000) - 60 * minutes;
  return `${minutesText}${seconds}s`;
};

updateTime = (timeToGame, elementId) => {
  console.log("update time", timeToGame);
  const timeText = formatTime(timeToGame);
  document.getElementById(elementId).innerText = timeText;
};

fetchGameText = async () => {
  try {
    const headers = new Headers();
    headers.append("Content-Type", "application/json");
    headers.append("Authorization", `Bearer ${jwt}`);

    const response = await fetch("http://localhost:3000/text", {
      method: "GET",
      headers,
      mode: "cors",
      cache: "default"
    });

    const { gameText } = await response.json();
    return gameText;
  } catch (err) {
    console.log(err);
    return {
      error: err
    };
  }
};

displayPageBlocks = selectedPageBlockIds => {
  pageBlockIds.forEach(id => {
    document.getElementById(id).style.display = "none";
  });
  selectedPageBlockIds.forEach(id => {
    document.getElementById(id).style.display = "block";
  });
};

updateGameText = game => {
  const { correctSymbols, gameText } = game;
  const { text } = gameText;
  const enteredText = correctSymbols ? text.slice(0, correctSymbols) : "";
  let currentText = text.slice(correctSymbols, correctSymbols + 1);
  if (currentText === " ") {
    currentText = "_";
  }
  const remainingText =
    correctSymbols < text.length
      ? text.slice(correctSymbols + 1, text.length)
      : "";
  console.log("PARTS", enteredText, currentText, remainingText);

  document.getElementById("entered-text").innerText = enteredText;
  document.getElementById("current-text").innerText = currentText;
  document.getElementById("remaining-text").innerText = remainingText;
};

// process keystrokes
handleTyping = evt => {
  const { key } = evt;
  const { errorSymbols, gameText } = game;
  let { correctSymbols } = game;
  const { text } = gameText;
  const currentSymbol = text.slice(correctSymbols, correctSymbols + 1);
  if (key === currentSymbol) {
    correctSymbols++;
    const update = {
      token: jwt,
      correctSymbols,
      errorSymbols,
      id: socket.id,
      finished: correctSymbols === text.length
    };
    gameSocket.emit("update", update);
    game.correctSymbols = correctSymbols;
  } else {
    game.errorSymbols++;
  }
  updateGameText(game);
};

createPlayerView = player => {
  const containerDiv = document.createElement("div");
  containerDiv.className = "progress__display";

  const playerNameDiv = document.createElement("div");
  playerNameDiv.className = "progress__player";
  playerNameDiv.innerText = player.id;

  const progressDiv = document.createElement("div");
  progressDiv.className = "progress";

  const progressBarDiv = document.createElement("div");
  progressBarDiv.setAttribute("data-player-id", player.id);
  progressBarDiv.className = "progress-bar progress__value";
  progressBarDiv.setAttribute("role", "progressbar");
  progressBarDiv.setAttribute("style", "width: 0%;");
  progressBarDiv.setAttribute("aria-valuenow", "0");
  progressBarDiv.setAttribute("aria-valuemax", "100");
  progressBarDiv.innerText = "";

  progressDiv.append(progressBarDiv);

  containerDiv.append(playerNameDiv, progressDiv);
  return containerDiv;
};

updatePlayerProgress = player => {
  const { correctRate } = player;
  const displayRate = Math.floor(correctRate * 100);
  const playerProgressBars = [].slice.call(
    document.querySelectorAll("#progress .progress__value")
  );
  const playerProgressBar = playerProgressBars.find(
    view => view.getAttribute("data-player-id") === player.id
  );
  if (playerProgressBar) {
    playerProgressBar.setAttribute("style", `width: ${displayRate}%;`);
    playerProgressBar.setAttribute("aria-valuenow", `${displayRate}`);
    playerProgressBar.setAttribute("aria-valuemax", "100");
    playerProgressBar.innerText = `${displayRate || ""}%`;
  } else {
    throw new Error(`View for player ${player.id} is missing`);
  }
};

createPlayersView = players => {
  // clear view from previous game
  const parentContainer = document.querySelector("#progress .card-body");
  while (parentContainer.firstChild) {
    parentContainer.removeChild(parentContainer.firstChild);
  }

  const playerContainer = document.querySelector("#progress .card-body");
  Object.keys(players).forEach(id => {
    const playerView = createPlayerView(players[id]);
    playerContainer.appendChild(playerView);
  });
};

// update results
displayUpdates = players => {
  Object.keys(players).forEach(id => updatePlayerProgress(players[id]));
};

// handle game finish
displayResults = () => {};
