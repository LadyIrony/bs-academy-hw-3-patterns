formToTextBody = formId => {
  //   todo: find out why FormData is not working properly
  //   const form = document.forms[formId];
  //   const formData = new FormData(form);
  //   const data = {};
  //   for (let [key, prop] of formData) {
  //     data[key] = prop;
  //   }

  const email = document.getElementById("inputEmail").value.toLowerCase();
  const password = document.getElementById("inputPassword").value;
  return JSON.stringify({
    email,
    password
  });
};

handleLogin = async evt => {
  evt.preventDefault();

  const headers = new Headers();
  headers.append("Content-Type", "application/json");

  try {
    const response = await fetch("http://localhost:3000/login", {
      method: "POST",
      headers,
      mode: "cors",
      cache: "default",
      body: formToTextBody("login")
    });

    const authDetails = await response.json();

    if (authDetails.auth) {
      localStorage.setItem("jwt", authDetails.token);
      document.getElementById("auth-failed").style.display = "none";
      location.replace("/race");
    } else {
      document.forms["login"].reset();
      document.getElementById("auth-failed").style.display = "block";
    }
  } catch (err) {
    console.log(err);
    document.getElementById("auth-error").style.display = "block";
  }
  return false;
};

window.onload = () => {
  const loginForm = document.getElementById("login");
  loginForm.onsubmit = handleLogin;
};
