const texts = require("./texts");
const textsCount = texts.length;

getRandomInRange = (min, max) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

module.exports = currentTextId => {
  let newTextId = currentTextId;
  while (newTextId === currentTextId) {
    newTextId = getRandomInRange(0, textsCount);
  }
  return {
    textId: newTextId,
    text: texts[newTextId]
  };
};
